<!DOCTYPE html>
<html lang='en'>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="color_form.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>

    <title>Regist</title>
</head>

<body>
    <?php
    session_start();
    // Code PHP xử lý validate
    $error = array();
    $data = array();
    $action = '';
    $date = date("YmdHis");
    $_SESSION = $_POST;

    if (!empty($_POST['register'])) {
        // Lấy dữ liệu
        $_SESSION['name'] = isset($_POST['name']) ? $_POST['name'] : '';
        $_SESSION['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
        $_SESSION['depart'] = isset($_POST['depart']) ? $_POST['depart'] : '--Chọn--';
        $_SESSION['birthDate'] = isset($_POST['birthDate']) ? $_POST['birthDate'] : '';

        // Kiểm tra định dạng dữ liệu
        if (empty($_POST['name'])) {
            $error['name'] = 'Hãy nhập tên.';
        }

        if (empty($_POST['gender'])) {
            $error['gender'] = 'Hãy chọn giới tính';
        }

        if (empty($_POST['depart']) || $_POST['depart'] == '--Depart--') {
            $error['depart'] = 'Hãy chọn phân khoa.';
        }

        if (empty($_POST['birth'])) {
            $error['birth'] = 'Hãy nhập ngày sinh';
        }

        //Thư mục bạn sẽ lưu file upload
        $target_dir    = "upload/";
        if (!file_exists($target_dir)) {
            mkdir($target_dir);
        }
        $filename = $_FILES["image"]["tmp_name"];
        $prod = "img";
        $extension = pathinfo(basename($_FILES["image"]["name"]), PATHINFO_EXTENSION); // jpg
        $newfilename = $prod . "_" . $date . "." . $extension;
       
        $target_file   = $target_dir . basename($newfilename);

        if (empty($error)) {
            move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
            $_SESSION['image'] = $target_file;
            header("Location: ./do_regist.php ");
        }
    }
    ?>
    <form method="post" action="" enctype="multipart/form-data" action=?#?>
	<div class="form_center">	
	    <table class="table_form">
			<tr>
			<td>
			<?php echo isset($error['name']) ? $error['name'] : ''; ?> <br />
                <?php echo isset($error['gender']) ? $error['gender'] : ''; ?> <br />
                <?php echo isset($error['depart']) ? $error['depart'] : ''; ?> <br />
                <?php echo isset($error['birth']) ? $error['birth'] : ''; ?> <br />

</td>
     
			</tr>
           
				<tr>
				<td><div class="form_td">Họ và tên<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div> </td>
					<td ><input type="text" class="input_form" name="name" size="30"></td>
				</tr>

                <tr>
				<td><div class="form_td">Giới tính<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
				<td> 
                    <?php
                    $Gender = array("Nam", "Nữ");
                    $keys = array_keys($Gender);
                    for ($i = 0; $i <= count($Gender) - 1; $i++) { ?>
                        <input type="radio" name="gender" checked="<?php echo "checked"; ?>" value=" <?php echo $Gender[$i]; ?>"> <?php echo $Gender[$i]; ?>
                    <?php } ?>

					</td>
					</tr>
				</tr>
				<td><div class="form_td">Phân Khoa<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
					<td>
					<select class="select" name="depart">
                        <option>--Chọn--</option>
                        <?php $depart= array("null" => " ", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                        foreach ($depart as $key => $value) { ?>
                            <option value="<?= $value ?>"><?= $value ?></option>
                        <?php } ?>
                    </select></td>
				</tr>

                <tr>
				<td><div class="form_td">Ngày sinh<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
                    <td>
                    <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                        <input class="form-control" readonly="" type="text" name="birth">
                        <span class="input-group-addon">
                        </span>
                    </div>
					</td>
				</tr>
				<tr>
				<td><div class="form_td">Địa chỉ<sup class="sup"></sup></div>
					<td ><input type="text" class="input_form" name="address" size="30"></td>
				</tr>

				<tr><td>
					<div for="avatar" class="form_td">Hình ảnh<sup class="sup"></sup></div>
                   <td>   <input type='file' id='image' name='image' accept=".jpg,.jpeg,.png">

					</td>
				</tr>

				<tr>
				<td colspan="2" align="center">
				<input type='submit' class="form_td" name="register" value='Đăng ký' />
					</td>
				</tr>

						</table>
    </form>
    <script type="text/javascript">
        $(function() {
            $('#datepicker').datepicker();
        });
    </script>
</body>

</html>
