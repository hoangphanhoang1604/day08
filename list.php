<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <link rel="stylesheet" href="color_form.css">
   
    <title>Danh sách sinh viên</title>
</head>

<body>
    <?php
    session_start();
    $depart = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
    
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $_SESSION = $_POST;
    }

    if (!empty($_POST['add'])) {
        header("Location: ./regist.php ");
    }
    if (!empty($_POST['search'])) {
        $_SESSION['depart'] = isset($_POST['depart']) ? $_POST['depart'] : '';
        $_SESSION['note'] = isset($_POST['note']) ? $_POST['note'] : '';
        header("Location: ./danhsach_cf.php ");
    }

    if (!empty($_POST['add'])) {
        header("Location: ./regist.php ");
    }
    ?>
    <div class="form_center">
    	
        <form id="FormConfirm" method="POST" enctype='multipart/form-data'  action="<?php echo $_SERVER["PHP_SELF"];?>">			
       <table class="form_center" align="center" >
       <tr>
           <td><div>Khoa<sup class="sup">
        </sup></div> </td>
               <td >
               <select id="depart" name="depart">
                            <option value="0"></option>
                            <?php
                                foreach($depart as $x => $x_value) {
                                    if ($_SESSION["depart"] == $x_value) {
                                        echo '<option selected value="' . $x_value . '">' . $x_value . '</option>';
                                    }
                                    else echo '<option value="' . $x_value . '">' . $x_value . '</option>';
                                }
                            ?>
                        </select>

              </td>
           </tr>
           
           <td><div>Từ Khóa<sup class="sup" >
        </sup></div> </td>
               <td for="note" > 
               <?php 
                 if ($_SESSION["note"] != "") {
                                echo '<input type="text" name="note" id="note" class="field__input" value="' . $_SESSION["note"] .'"/>';
                            }
                            else echo '<input type="text" name="note" id="note" class="field__input"/>';
                        ?>

</td>
           </tr>        
      
           <td><sup class="sup">
        </sup></div> </td>
               <td> <input type='submit' class="form_td" name="search" value='Tìm kiếm'></td>
               <td><input type="submit" class="form_td" onclick="clearData()" value="    Xóa    "> </td>
           </tr>   
       
       
       <td><div>Số sinh viên tìm thấy: XXX<sup class="sup">
        </sup></div> </td>
        <td>

        </td>
        <td></td>
               <td > <input type='submit' class="form_td" name="add" value='Thêm' /></td>
           </tr>      
           <tr>
               <td>No</td>
               <td>Tên sinh viên</td>
               <td>Khoa</td>
               <td>Action</td>
           </tr>

           <tr>
               <td>1</td>
               <td>Nguyễn Văn A</td>
               <td>Khoa học máy tính</td>
               <td>
                   <input type='submit' onclick="myFunction()" class="form_td" name="delete" value='Xóa' />
               </td>
               <td>
                   <input type='submit' class="form_td" name="update" value='Sửa' />
               </td>
           </tr>

           <tr>
               <td>2</td>
               <td>Trần Thị B</td>
               <td>Khoa học máy tính</td>
               <td>
                   <input type='submit' class="form_td" name="delete" value='Xóa' />
               </td>
               <td>
                   <input type='submit' class="form_td" name="update" value='Sửa' />
               </td>
           </tr>

           <tr>
               <td>3</td>
               <td>Nguyễn Hoàng C</td>
               <td>Khoa học vật liệu</td>
               <td>
                   <input type='submit' class="form_td" name="delete" value='Xóa' />
               </td>
               <td>
                   <input type='submit' class="form_td" name="update" value='Sửa' />
               </td>
           </tr>

           <tr>
               <td>4</td>
               <td>Đinh Quang D</td>
               <td>Khoa học vật liệu</td>
               <td>
                   <input type='submit' class="form_td" name="delete" value='Xóa' />
               </td>
               <td>
                   <input type='submit' class="form_td" name="update" value='Sửa' />
               </td>
           </tr>

       </table>
                   </table>
</form>
<script>
   function myFunction() {
       document.getElementById("form").reset();
   }
   const depart = document.getElementById("depart");
    const note = document.getElementById("note");

    function clearData() {
        depart.value = "0";
        note.value = "";
    };

</script>

</body>

</html>